class Ship:
    
    def __init__(self, sz, snd):
        self.size = sz
        self.sailSnd = snd
        pass

    def getSize(self):
        return self.size
    
    def setSize(self,sz):
        self.size = sz
    
    def sail(self):
        print(self.sailSnd)
        pass
    
class Submarine(Ship):
    def __init__(self):
        Ship.__init__(self,2,"blubb blubb")
        
    """ Submarines print blubb blubb blubb on sail(). They have size 2."""
    pass

class Battleship(Ship):
    def __init__(self):
        Ship.__init__(self,4,"steam steam steam")
    """ Battleships print steam steam steam on sail(). They have size 4. """
    pass

class Carrier(Ship):
    def __init__(self):
		#Modified by AD to increasse size -
        # More edits by AD
        Ship.__init__(self,8,"whooooaaaaaa")
        """ Carriers print whooooaaaaaa on sail(). They have size 7. They can launch aircraft."""
    pass
    def launch_aircraft(self):
        print("whooosh - done!")
